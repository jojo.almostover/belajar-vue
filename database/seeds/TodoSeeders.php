<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TodoSeeders extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker\Generator $faker)
    {
        for ($i=0;$i<10;$i++){
                DB::table('todo')->insert([
                    'todo'=> $faker->name
                ]);
        }
    }
}
