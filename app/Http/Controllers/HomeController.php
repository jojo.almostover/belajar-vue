<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller implements sembarang
//extend mamangil induknya brarti controller sebagai  induknya, untuk memanggil hasil extends menggunakan this, lebih tepatnya extends adalah append koding dari sebelumnya
//implement adalah interface seperti tiap klass membutukan kriteria tertentu

//public bisa dipanggil dimana mana
//private hanya bisa dipanggil di kelas itu sendiri
//protected hanya bisa dipanggil di kelas sendiri dan anake e alias dalame extends
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public  function ngitung($a,$b){

        return $a+$b;
    }

    function test()
    {
        // TODO: Implement test() method.
    }
}
//contoh implement
interface  sembarang
{
    function test();
}

//contoh memanggil objek klas
$a = new HomeController();

$g=$a->ngitung(1,2);
$h=$a->ngitung(3,4);

$sc= new ScheduleController();
$sc->index();